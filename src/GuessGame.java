public class GuessGame {

    public void startGame() {
       Player p1 = new Player();
       Player p2 = new Player();
       Player p3 = new Player();
        boolean p1isRight = false;
        boolean p2isRight = false;
        boolean p3isRight = false;
        int targetNumber = (int) (Math.random() * 10);
        System.out.println("Я загадал число от 1 до 9...");
        while (true) {
            System.out.println("Загаданное число это... " + targetNumber);

            p1.guess();
            p2.guess();
            p3.guess();

            System.out.println("Игрок один отгадывает " + p1.getNumber() +
                    "\n Игрок два отгадывает " + p2.getNumber() +
                    "\n Игрок три отгадывает " + p3.getNumber());

            if (p1.getNumber() == targetNumber) {
                p1isRight = true;
            } else if (p2.getNumber() == targetNumber) {
                p2isRight = true;
            } else if (p3.getNumber() == targetNumber) {
                p3isRight = true;
            }

            if (p1isRight || p2isRight || p3isRight) {
                System.out.println("У нас есть победитель! \n " +
                        "Отгадал ли третий игрок? " + p1isRight +
                        "\n Отгадал ли второй игрок? " + p2isRight +
                        "\n Отгадал ли третий игрок? " + p3isRight +
                        "\n Игра окончена");
                break;
            } else {
                System.out.println("Игроки попытаются отгадать снова");
            }
        }
    }
}
